package com.massarttech.android.translationdemo;

import android.app.Application;

import com.massarttech.android.translation.Language;
import com.massarttech.android.translation.TranslationHelper;
import com.massarttech.android.translation.TranslationOptions;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        TranslationHelper.initialize(this, TranslationOptions.newBuilder("http://192.168.1.7:8090/api/")
                .autoDetectLanguage()
                .addLanguage(Language.ENGLISH)
                .addLanguage(Language.ARABIC)
                .addLanguage(Language.URDU)
                .build());
    }
}
