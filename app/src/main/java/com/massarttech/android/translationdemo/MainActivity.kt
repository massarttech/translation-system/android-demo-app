 package com.massarttech.android.translationdemo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.massarttech.android.translation.TranslationHelper

 class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        TranslationHelper.loadTranslations {
            TranslationHelper.doTranslations(this, "LANGUAGE_ACTIVITY_P")
        }
    }
}
